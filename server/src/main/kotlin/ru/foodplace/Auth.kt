package ru.foodplace

import io.javalin.core.security.Role
import io.javalin.http.Context
import io.javalin.http.Handler
import ru.foodplace.users.UsersDao

enum class ApiRole : Role { ANYONE, CUSTOMER, MANUFACTURER }

object Auth {

    lateinit var dao: UsersDao

    fun accessManager(handler: Handler, ctx: Context, permittedRoles: Set<Role>) {
        when {
            permittedRoles.contains(ApiRole.ANYONE) -> handler.handle(ctx)
            ctx.userRoles.any { it in permittedRoles } -> handler.handle(ctx)
            else -> ctx.status(401).json("Unauthorized")
        }
    }

    // get roles from userRoleMap after extracting username/password from basic-auth header
    private val Context.userRoles: List<ApiRole>
        get() = this.basicAuthCredentials()?.let { (username, password) ->
            dao.getUserRoles(username, password)
        } ?: emptyList()



}