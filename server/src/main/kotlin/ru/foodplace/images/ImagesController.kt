package ru.foodplace.images

import io.javalin.http.Context
import io.javalin.http.Handler
import java.io.File
import java.io.FileInputStream

object ImagesController {

    var images = File("images")

    fun getImage(ctx: Context) {
        ctx.result(FileInputStream(File(images, ctx.pathParam("image-id"))))
    }

}