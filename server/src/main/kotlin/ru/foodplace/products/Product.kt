package ru.foodplace.products

data class Product(val id: Long, val name: String, val price: Int, val categoryId: Int, val quantity: Int, val unit: String)