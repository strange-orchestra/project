package ru.foodplace.products

import ru.foodplace.DBCPDataSource
import ru.foodplace.DBCPDataSource.create

class ProductsDao {

    fun getProduct(id: Long) : Product? {
        val r = create.fetch("SELECT * FROM product WHERE id = $id").firstOrNull()
        r?.let {
            return Product(r["id"] as Long, "", r["price"] as Int, r["category_id"] as Int, r["quantity"] as Int, r["unit"] as String)
        } ?: return null
    }

}