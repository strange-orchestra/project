package ru.foodplace.products

import io.javalin.http.Context

object ProductsController {

    val dao = ProductsDao()

    fun getAllProducts(ctx: Context) {

    }

    fun getProduct(ctx: Context) {
        val product = dao.getProduct(ctx.pathParam("product-id").toLong())
        if (product != null) {
            ctx.json(product)
        } else {
            ctx.status(204)
            ctx.result("Not found")
        }
    }

    fun searchProducts(ctx: Context) {

    }

}