package ru.foodplace

import java.math.BigDecimal

data class Address(val cityId: Int, val latitude: BigDecimal, val longitude: BigDecimal, val title: String)