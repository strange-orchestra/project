package ru.foodplace

import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.core.security.SecurityUtil.roles
import ru.foodplace.categories.CategoriesController
import ru.foodplace.cities.CitiesController
import ru.foodplace.images.ImagesController
import ru.foodplace.products.ProductsController
import ru.foodplace.users.UsersController
import ru.foodplace.users.UsersDao

fun main(args: Array<String>) {

    val usersDao = UsersDao()
    UsersController.dao = usersDao
    Auth.dao = usersDao

    val app = Javalin.create {
        it.accessManager(Auth::accessManager)
    }.apply {
        exception(Exception::class.java) { e, ctx -> e.printStackTrace() }
    }.start(7000)

    app.routes {
        get("/images/:image-id", ImagesController::getImage, roles(ApiRole.ANYONE))
        path("cities") {
            get(CitiesController::getAllCities, roles(ApiRole.ANYONE))
            path(":city-id") {
                get(CitiesController::getCityDeliveryPoints, roles(ApiRole.CUSTOMER))
            }
        }
        path("products") {
            get(ProductsController::getAllProducts, roles(ApiRole.ANYONE))
            path(":product-id") {
                get(ProductsController::getProduct, roles(ApiRole.ANYONE))
            }
            post(ProductsController::searchProducts, roles(ApiRole.ANYONE))
        }
        path("categories") {
            get(CategoriesController::getAllCategories, roles(ApiRole.ANYONE))
        }
        path("users") {
            post(UsersController::registerUser, roles(ApiRole.ANYONE))
            path(":user-id") {
                path("cart") {
                    get(UsersController::getUserCart, roles(ApiRole.CUSTOMER))
                }
            }
        }
    }
}
