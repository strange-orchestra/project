package ru.foodplace.categories

data class Category(val id: Int, val title: String)