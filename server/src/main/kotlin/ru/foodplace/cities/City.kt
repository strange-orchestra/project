package ru.foodplace.cities

data class City(val id: Int, val name: String)