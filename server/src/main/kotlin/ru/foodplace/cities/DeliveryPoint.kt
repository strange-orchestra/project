package ru.foodplace.cities

import ru.foodplace.Address

data class DeliveryPoint(val id: Long, val address: Address)