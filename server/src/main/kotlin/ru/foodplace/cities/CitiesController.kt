package ru.foodplace.cities

import io.javalin.http.Context

object CitiesController {

    var dao = CitiesDao()

    fun getAllCities(ctx: Context) {
        ctx.json(dao.getCities())
    }

    fun getCityDeliveryPoints(ctx: Context) {
        ctx.json(dao.getCityDeliveryPoints(ctx.pathParam("city-id").toInt()))
    }

}