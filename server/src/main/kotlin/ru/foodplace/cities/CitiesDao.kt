package ru.foodplace.cities

import ru.foodplace.Address
import ru.foodplace.DBCPDataSource.create
import java.math.BigDecimal
import java.util.stream.Collectors

class CitiesDao {

    val CITY = "city"

    fun getCities() : Array<City> {
        val result = create.select().from(CITY).fetch()
        return result.map { r -> City(r["id"] as Int, r["name"] as String) }.toTypedArray()
    }

    fun getCityDeliveryPoints(cityId: Int) : Array<DeliveryPoint> {
        val result = create.fetch("SELECT delivery_point.id AS id, address.city_id AS city_id, address.latitude AS latitude, address.longitude AS longitude, address.name AS title FROM delivery_point JOIN address ON delivery_point.address_id = address.id WHERE city_id = $cityId")
        return result.map {
                r -> DeliveryPoint(r["id"] as Long, Address(r["city_id"] as Int, r["latitude"] as BigDecimal, r["longitude"] as BigDecimal, r["title"] as String))
            }
            .toTypedArray()
    }

}