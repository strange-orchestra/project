package ru.foodplace.users

import org.apache.commons.codec.digest.DigestUtils
import ru.foodplace.ApiRole
import ru.foodplace.DBCPDataSource.create

class UsersDao {

    val USER = "user"

    fun getUserRoles(username: String, password: String): List<ApiRole> {
        val user = create.select().from(USER).where("login = '$username'").fetch().firstOrNull()
        user?.let {
            if (user["hashed_password"] == DigestUtils.sha1Hex(password)) {
                return listOf(ApiRole.valueOf(user["role"] as String))
            }
        }
        return emptyList()
    }

}