package ru.foodplace

import org.apache.commons.dbcp2.BasicDataSource
import org.jooq.DSLContext
import org.jooq.impl.DSL
import java.sql.Connection
import java.sql.SQLException
import java.util.*


object DBCPDataSource {

    private val ds = BasicDataSource()

    val connection: Connection
        @Throws(SQLException::class)
        get() = ds.getConnection()

    val create: DSLContext
        get() = DSL.using(connection)

    init {
        val properties = Properties()
        properties.load(Properties::class.java.getResourceAsStream("/config.properties"))
        ds.driverClassName = "com.mysql.jdbc.Driver"
        ds.url = properties.getProperty("db.url")
        ds.username = properties.getProperty("db.username")
        ds.password = properties.getProperty("db.password")
//        ds.setMinIdle(5)
//        ds.setMaxIdle(10)
//        ds.setMaxOpenPreparedStatements(100)
    }
}