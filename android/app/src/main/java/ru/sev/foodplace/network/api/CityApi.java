package ru.sev.foodplace.network.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.sev.foodplace.data.entity.City;

public interface CityApi {

    @GET("/cities")
    Call<List<City>> getAllCities();

    @GET("/cities/{id}")
    Call<City> getCityById(@Path("id") int id);

}
