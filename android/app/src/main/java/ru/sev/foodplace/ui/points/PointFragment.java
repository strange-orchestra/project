package ru.sev.foodplace.ui.points;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.sev.foodplace.R;
import ru.sev.foodplace.ui.base.BaseFragment;
import ru.sev.foodplace.ui.points.children.ListPointFragment;
import ru.sev.foodplace.ui.points.children.MapFragment;

public class PointFragment extends BaseFragment {

    private ListPointFragment listPointFragment;
    private MapFragment mapFragment;
    private PointFragmentAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new PointFragmentAdapter(getChildFragmentManager());
        listPointFragment = new ListPointFragment();
        mapFragment = new MapFragment();
        adapter.addFragment(mapFragment, "Карта");
        adapter.addFragment(listPointFragment, "Список");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.points_layout, container, false);
//        setRetainInstance(true);

        viewPager = mView.findViewById(R.id.points_viewPager);
        tabLayout = mView.findViewById(R.id.points_tabLayout);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        return mView;
    }

    public void showData() {

    }
}
