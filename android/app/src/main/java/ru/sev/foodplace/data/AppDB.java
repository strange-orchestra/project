package ru.sev.foodplace.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.sev.foodplace.data.dao.ProductDAO;
import ru.sev.foodplace.data.entity.Category;
import ru.sev.foodplace.data.entity.Product;
import ru.sev.foodplace.data.entity.ProductPoint;

@Database(entities = {Product.class, ProductPoint.class, Category.class}, version = 1, exportSchema = false)
public abstract class AppDB extends RoomDatabase {
    ProductDAO productDAO;
}
