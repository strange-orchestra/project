package ru.sev.foodplace.ui.set;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ru.sev.foodplace.R;
import ru.sev.foodplace.data.entity.City;

public class SetCityActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @SuppressLint("StaticFieldLeak")
    private static SetCityActivity instance;

    public CityAdapter adapter;

    public static SetCityActivity getInstance() {
        return instance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_city);

        instance = this;
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        adapter = new CityAdapter();
        adapter.setCities(getCityList());

        recyclerView = findViewById(R.id.text_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        NetworkService.getInstance().getCityApi().getAllCities().enqueue(
//                new Callback<List<City>>() {
//                    @Override
//                    public void onResponse(Call<List<City>> call, Response<List<City>> response) {
//                        adapter.setCities(response.body());
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<City>> call, Throwable t) {
//                        Toast.makeText(SetCityActivity.this, "Проблема с сетью", Toast.LENGTH_SHORT).show();
//
//                        if (SetCityActivity.getInstance() != null)
//                        SetCityActivity.getInstance().onBackPressed();
//                    }
//                }
//        );

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public List<City> getCityList() {
        City sev = new City();
        sev.id = 1;
        sev.name = "Севастополь";
        City simf = new City();
        simf.id = 2;
        simf.name = "Симферополь";
        ArrayList<City> cities = new ArrayList<>();
        cities.add(sev);
        cities.add(simf);
        return cities;
    }
}
