package ru.sev.foodplace.data.entity;

public class Manufacturer {
    public int id;
    public String name;
    public String description;
}
