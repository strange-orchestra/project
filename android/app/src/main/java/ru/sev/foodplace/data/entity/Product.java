package ru.sev.foodplace.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Product {

    @PrimaryKey(autoGenerate = true)
    public int roomId;

    public int id;

    public String manufacturerName;

    public int manufacturerId;

    public String name;

    public int price;

    public int categoryId;

    public String unit;

    public String imageUrl;

}
