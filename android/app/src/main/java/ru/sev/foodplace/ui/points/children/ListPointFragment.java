package ru.sev.foodplace.ui.points.children;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.sev.foodplace.R;
import ru.sev.foodplace.ui.base.BaseFragment;

public class ListPointFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_points_layout, container, false);

        return mView;
    }

    public void showData() {

    }
}
