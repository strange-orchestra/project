package ru.sev.foodplace.data.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ProductPoint {

    @PrimaryKey(autoGenerate = true)
    public int roomId;

    @Expose
    public int id;

    @Expose
    public int cityId;

    @SerializedName("title")
    @Expose
    public String address;

    @Expose
    public double latitude;

    @Expose
    public double longitude;


}
