package ru.sev.foodplace.ui.product;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ru.sev.foodplace.R;
import ru.sev.foodplace.data.entity.Product;
import ru.sev.foodplace.ui.MainActivity;
import ru.sev.foodplace.ui.set.CityAdapter;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> products;


    public ProductAdapter() {
        if (products == null) {
            products = new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new ProductAdapter.ProductViewHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item_product_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        holder.bind(products.get(position));
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView price;
        TextView name;
        TextView manufacturer;
        Button buttonBuy;


        @SuppressLint("SetTextI18n")
        public void bind(Product product) {

//            price.setText("" + product.price/100 + " руб.");
            price.setText("" + BigDecimal.valueOf(product.price/100).setScale(2).doubleValue() + " руб.");

            name.setText(product.name);
            manufacturer.setText(product.manufacturerName);

            Picasso.with(MainActivity.getInstance())
                    .load(Uri.parse(product.imageUrl))
                    .error(R.drawable.ic_launcher_def)
                    .into(imageView);

            buttonBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Toast.makeText(MainActivity.getInstance(), "Товар добавлен в корзину", Toast.LENGTH_SHORT).show();
                }
            });

        }

        public ProductViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView_news);
            price = itemView.findViewById(R.id.price_textView);
            name = itemView.findViewById(R.id.nameProduct);
            manufacturer = itemView.findViewById(R.id.manufacturer_textView);
            buttonBuy  = itemView.findViewById(R.id.buyButton);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
