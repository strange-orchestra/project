package ru.sev.foodplace.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraPosition;

import java.util.Objects;

import ru.sev.foodplace.App;
import ru.sev.foodplace.R;
import ru.sev.foodplace.data.entity.City;
import ru.sev.foodplace.ui.basket.BasketFragment;
import ru.sev.foodplace.ui.cabinet.CabinetFragment;
import ru.sev.foodplace.ui.points.PointFragment;
import ru.sev.foodplace.ui.product.ProductFragment;
import ru.sev.foodplace.ui.set.SetCityActivity;
import ru.sev.foodplace.utils.Auth;
import ru.sev.foodplace.utils.Defaults;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance;

    private BasketFragment basketFragment;

    private CabinetFragment cabinetFragment;

    private PointFragment pointFragment;
    public static CameraPosition cameraPosition;


    private ProductFragment productFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MapKitFactory.setApiKey(Defaults.MAP_KIT_API_KEY);
        MapKitFactory.initialize(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        basketFragment = new BasketFragment();
        cabinetFragment = new CabinetFragment();
        pointFragment = new PointFragment();
        productFragment = new ProductFragment();


        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        if (Auth.getCity() == 0) {
//            Intent intent = new Intent(App.getInstance(), SetCityActivity.class);
//            startActivity(intent);
//        }
//        else {
//            cameraPosition = new CameraPosition(new Point(App.dataOwner.city.lat, App.dataOwner.city.lng),   12.0f, 0.0f, 0.0f);
//        }

        if (Auth.getDefaultPoint() == 0) {

        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, productFragment).commit();
    }

    public void updateCameraPosition(City city) {
        cameraPosition = new CameraPosition(new Point(city.lat, city.lng),   12.0f, 0.0f, 0.0f);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

//            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            switch (item.getItemId()) {
                case R.id.navigation_products:
                    transaction.replace(R.id.fragment_container, productFragment).commit();
                    return true;

                case R.id.navigation_points:
                    transaction.replace(R.id.fragment_container, pointFragment).commit();
                    return true;

                case R.id.navigation_bucket:
                    transaction.replace(R.id.fragment_container, basketFragment).commit();
                    return true;

                case R.id.navigation_cabinet:
                    transaction.replace(R.id.fragment_container, cabinetFragment).commit();
                    return true;
            }
            return false;
        }
    };

    public static MainActivity getInstance() {
        return instance;
    }

    public static boolean isConnected() {
        NetworkInfo networkInfo = ((ConnectivityManager) Objects.requireNonNull(App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE))).getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (Auth.getCity() == 0) {
            Intent intent = new Intent(App.getInstance(), SetCityActivity.class);
            startActivity(intent);
        }
        else {
            cameraPosition = new CameraPosition(new Point(App.dataOwner.city.lat, App.dataOwner.city.lng),   12.0f, 0.0f, 0.0f);
        }
    }
}
