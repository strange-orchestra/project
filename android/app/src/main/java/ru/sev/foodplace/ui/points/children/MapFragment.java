package ru.sev.foodplace.ui.points.children;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.map.CameraListener;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.CameraUpdateSource;
import com.yandex.mapkit.map.Map;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.mapkit.user_location.UserLocationLayer;
import com.yandex.runtime.image.ImageProvider;

import java.util.Objects;

import ru.sev.foodplace.App;
import ru.sev.foodplace.data.entity.ProductPoint;
import ru.sev.foodplace.ui.MainActivity;
import ru.sev.foodplace.R;
import ru.sev.foodplace.ui.base.BaseFragment;

public class MapFragment extends BaseFragment implements CameraListener {

    private MapView mapView;
    private UserLocationLayer userLocationLayer;
    private MapObjectCollection mapObjects;
//    private CameraPosition cameraPosition;

    private MapObjectTapListener tapListener = new MapObjectTapListener() {
        @Override
        public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {
            Toast.makeText(App.getInstance(), "Точка выбрана!", Toast.LENGTH_SHORT).show();
            return false;
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.map_layout, container, false);

        mapView = mView.findViewById(R.id.mapView);
        mapObjects = mapView.getMap().getMapObjects().addCollection();


        return mView;
    }


    public void showData() {
        mapObjects.clear();

        for (ProductPoint productPoint : App.dataOwner.productPoints) {

            Point objectPoint = new Point(productPoint.latitude, productPoint.longitude);

            PlacemarkMapObject mark = mapObjects.addPlacemark(objectPoint);
            mark.setDraggable(false);
            mark.setIcon(ImageProvider.fromResource(Objects.requireNonNull(getContext()), R.drawable.map_marker));

            mark.addTapListener(tapListener);
        }
    }

    @Override
    public void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
        mapView.getMap().addCameraListener(this);

        if (MainActivity.cameraPosition != null) {
            mapView.getMap().move(MainActivity.cameraPosition);
        }
        super.onStart();
    }

    @Override
    public void onCameraPositionChanged(@NonNull Map map, @NonNull CameraPosition cameraPosition, @NonNull CameraUpdateSource cameraUpdateSource, boolean b) {
        MainActivity.cameraPosition = cameraPosition;
    }
}
