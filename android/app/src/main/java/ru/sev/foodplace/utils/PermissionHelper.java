package ru.sev.foodplace.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import ru.sev.foodplace.App;
import ru.sev.foodplace.ui.MainActivity;

public class PermissionHelper {
    public static final int REQUEST_CODE_PERMISSION_CAMERA = 1;
    public static final  int REQUEST_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE = 2;
    public static final  int REQUEST_CODE_PERMISSION_ALL = 3;



    public static boolean isCoarseLocationAccess() {
        return ContextCompat.checkSelfPermission(App.getInstance(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isFineLocationAccess() {
        return ContextCompat.checkSelfPermission(App.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isWriteExternalStorageAccess() {
        return ContextCompat.checkSelfPermission(App.getInstance(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestWriteExternalPermission(){
        ActivityCompat.requestPermissions(MainActivity.getInstance(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE);
    }


    public static void requestAllPermission(){
        ActivityCompat.requestPermissions(MainActivity.getInstance(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_PERMISSION_ALL);
    }

    private PermissionHelper() {

    }
}
