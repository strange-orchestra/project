package ru.sev.foodplace.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.sev.foodplace.data.entity.Product;

@Dao
public interface ProductDAO {
    @Query("SELECT * FROM Product")
    List<Product> getAll();

//    @Query("SELECT * FROM Product WHERE objectId = :objectId")
//    Product getById(String objectId);

    @Insert
    void insert(Product product);

    @Insert
    void insert(List<Product> product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdate(Product product);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdate(List<Product> products);

    @Update
    void update(Product product);

    @Update
    int update(List<Product> product);


    @Delete
    void delete(Product product);

    @Query("DELETE FROM product")
    void clearTable();


}
