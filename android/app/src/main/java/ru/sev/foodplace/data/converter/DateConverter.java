package ru.sev.foodplace.data.converter;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class DateConverter {

    @TypeConverter
    public Date fromTime(Long dateLong) {
        return dateLong == null ? null : new Date(dateLong);
    }

    @TypeConverter
    public Long toTime(Date date) {
        return date == null ? null : date.getTime();
    }
}
