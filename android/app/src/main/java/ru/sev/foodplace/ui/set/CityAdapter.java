package ru.sev.foodplace.ui.set;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.sev.foodplace.R;
import ru.sev.foodplace.data.entity.City;
import ru.sev.foodplace.utils.Auth;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityHolder> {

    private List<City> cities;


    public void setCities(List<City> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CityHolder(LayoutInflater.from(
                parent.getContext()).inflate(R.layout.simple_item_text_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CityHolder holder, int position) {
        holder.bind(cities.get(position));
    }

    @Override
    public int getItemCount() {
        if (cities != null) {
            return cities.size();
        } else {
            return 0;
        }
    }

    public class CityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nameCityTextView;

        public CityHolder(View itemView) {
            super(itemView);
            nameCityTextView = itemView.findViewById(R.id.city_name_TextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                City city = cities.get(position);
                Auth.setCity(city.id);
                Auth.setCityName(city.name);
                SetCityActivity.getInstance().onBackPressed();
            }
        }

        public void bind(City city) {
            nameCityTextView.setText(city.name);
        }
    }
}
