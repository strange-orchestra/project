package ru.sev.foodplace.network.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ru.sev.foodplace.data.entity.Product;

public interface ProductApi {

        @GET("cities/{city_id}/products")
        Call<List<Product>> getAllProducts(@Path("city_id") int cityId);

        @GET("/products/{id}")
        Call<Product> getProductById(@Path("id") int id);


}
