package ru.sev.foodplace.ui.base;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment {
    protected View mView;

    public BaseFragment() {

    }

    public abstract void showData();

    @Override
    public void onStart() {
        super.onStart();
        showData();
    }
}
