package ru.sev.foodplace.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import ru.sev.foodplace.App;

public class Auth {

    private static SharedPreferences getSP() {
        return App.getInstance().getSharedPreferences("auth", Context.MODE_PRIVATE);
    }

    public static int getCity() {
        return getSP().getInt("city", 0);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setCity(int cityId) {
         getSP().edit().putInt("city", cityId).apply();
    }

    public static String getCityName() {
        return getSP().getString("cityName", null);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setCityName(String cityName) {
        getSP().edit().putString("cityName", cityName).apply();
    }

    public static int getDefaultPoint() {
        return getSP().getInt("productPoint", 1);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setDefaultPoint(int pointId) {
        getSP().edit().putInt("productPoint", pointId).apply();
    }

    public static String getLogin() {
        return getSP().getString("login", null);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setLogin(String login) {
        getSP().edit().putString("login", login).apply();
    }

    public static String getPassword() {
        return getSP().getString("password", null);
    }

    @SuppressLint("CommitPrefEdits")
    public static void setPassword(String password) {
        getSP().edit().putString("password", password).apply();
    }
}
