package ru.sev.foodplace.data.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.sev.foodplace.data.entity.ProductPoint;

@Dao
public interface ProductPointDAO {
    @Query("SELECT * FROM productPoint")
    List<ProductPoint> getAll();

//    @Query("SELECT * FROM ProductPoint WHERE objectId = :objectId")
//    ProductPoint getById(String objectId);

    @Insert
    void insert(ProductPoint productPoint);

    @Insert
    void insert(List<ProductPoint> ProductPoint);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdate(ProductPoint productPoint);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOrUpdate(List<ProductPoint> productPoint);

    @Update
    void update(ProductPoint productPoint);

    @Update
    int update(List<ProductPoint> productPoint);


    @Delete
    void delete(ProductPoint productPoint);

    @Query("DELETE FROM ProductPoint")
    void clearTable();


}
