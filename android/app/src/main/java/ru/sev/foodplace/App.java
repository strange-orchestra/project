package ru.sev.foodplace;

import android.app.Application;
import android.arch.persistence.room.Room;

import ru.sev.foodplace.data.AppDB;
import ru.sev.foodplace.network.DataOwner;

public class App extends Application {
    private static App instance;

    private AppDB db;

    public static DataOwner dataOwner;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = Room.databaseBuilder(this, AppDB.class, "database").build();
        instance = this;

        dataOwner = new DataOwner();


    }
}
