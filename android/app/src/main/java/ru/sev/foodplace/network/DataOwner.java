package ru.sev.foodplace.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import ru.sev.foodplace.data.entity.Category;
import ru.sev.foodplace.data.entity.City;
import ru.sev.foodplace.data.entity.Manufacturer;
import ru.sev.foodplace.data.entity.Product;
import ru.sev.foodplace.data.entity.ProductPoint;

public class DataOwner {

    public City city;
    public List<Product> products;
    public List<ProductPoint> productPoints;
    public List<Category> categories;
    public List<Manufacturer> manufacturers;
    public Manufacturer manufacturer;

    public List<Product> basket;

    public DataOwner() {
        city = new City();
        city.name = "Севастополь";
        city.id = 1;
        city.lat = 44.616743;
        city.lng = 33.525651;


        productPoints = new ArrayList<>();
        products = new ArrayList<>();
        basket = new ArrayList<>();
        categories = new ArrayList<>();
        manufacturers = new ArrayList<>();

        initCategories();
        initManufactures();
        initProductPoints();
        initProducts();

    }

    private void initManufactures() {
        manufacturer = new Manufacturer();
        manufacturer.id = 1;
        manufacturer.description = "Занимаюсь продажей свежих фруктов и овощей уже более 10 лет. Будьте здоровы и питайтесь хорошей пищей, буду рада, если Вам понравится моя продукция";
        manufacturers.add(manufacturer);
    }

    private void initProductPoints() {
        ProductPoint point1 = new ProductPoint();
        point1.cityId = 1;
        point1.latitude = 44.608328;
        point1.longitude = 33.513325;
        point1.id = 1;
        point1.address = "Ул.Лоионосова, 8";

        ProductPoint point2 = new ProductPoint();
        point2.cityId = 1;
        point2.latitude = 44.598916;
        point2.longitude = 33.488117;
        point2.id = 1;
        point2.address = "площадь 50-летия СССР, 11";

        ProductPoint point3 = new ProductPoint();
        point3.cityId = 1;
        point3.latitude = 44.580017;
        point3.longitude = 33.518015;
        point3.id = 1;
        point3.address = "проспект Генерала Острякова, 62";

        ProductPoint point4 = new ProductPoint();
        point4.cityId = 1;
        point4.latitude = 44.595228;
        point4.longitude = 33.545321;
        point4.id = 1;
        point4.address = "улица Багрия, 48";

        ProductPoint point5 = new ProductPoint();
        point5.cityId = 1;
        point5.latitude = 44.589525;
        point5.longitude = 33.445429;
        point5.id = 1;
        point5.address = "проспект Героев Сталинграда, 52";

        productPoints.add(point1);
        productPoints.add(point2);
        productPoints.add(point3);
        productPoints.add(point4);
        productPoints.add(point5);
    }

    private void initCategories() {
        Category category1 = new Category();
        category1.id = 1;
        category1.name = "Овощи и фрукты";

        Category category2 = new Category();
        category2.id = 2;
        category2.name = "Мясо";

        Category category3 = new Category();
        category3.id = 3;
        category3.name = "Молочная";

        Category category4 = new Category();
        category4.id = 4;
        category4.name = "Морепродукты";

        Category category5 = new Category();
        category5.id = 5;
        category5.name = "Кондитерские изделия";

        Category category6 = new Category();
        category6.id = 5;
        category6.name = "Выпечка";


        categories.add(category1);
        categories.add(category2);
        categories.add(category3);
        categories.add(category4);
        categories.add(category5);
    }

    private void initProducts() {
        Product tomato = new Product();
        Product apple = new Product();
        Product fish = new Product();
        Product meat = new Product();
        Product cucumber = new Product();
        Product peach = new Product();

        tomato.categoryId = 1;
        tomato.id = 1;
        tomato.imageUrl = "http://profermu.com/wp-content/uploads/2017/10/0301w-15.jpg";
        tomato.name = "Помидор";
        tomato.unit = "кг";
        tomato.price = 5900;
        tomato.manufacturerId = 1;
        tomato.manufacturerName = "ИП Кобылинская И.А.";

        apple.categoryId = 1;
        apple.id = 2;
        apple.imageUrl = "https://riddle.su/icons/apple256.png";
        apple.name = "Яблоко Apple";
        apple.unit = "кг";
        apple.manufacturerId = 1;
        apple.manufacturerName = "ИП Кобылинская И.А.";
        apple.price = 8900;

        cucumber.categoryId = 1;
        cucumber.id = 3;
        cucumber.imageUrl = "https://mk0agronewsur597qvtv.kinstacdn.com/wp-content/uploads/2018/12/cucumber1597ed704d6141-623x340.jpeg";
        cucumber.name = "Огурец особенный";
        cucumber.unit = "кг";
        cucumber.manufacturerName = "ИП Необычный В.П.";
        cucumber.price = 3800;

        fish.categoryId = 4;
        fish.id = 3;
        fish.imageUrl = "https://fishingplanet.ru/common/upload/photo_gallery/magazine/2009/02/19/2009-02-19-29.jpg";
        fish.name = "Форель (Кумжа)";
        fish.manufacturerName = "ИП Рыбаковский И.А.";
        fish.unit = "кг";
        fish.price = 38000;

        meat.categoryId = 2;
        meat.id = 3;
        meat.imageUrl = "https://t-bone.ua/wp-content/uploads/2017/08/mramornaya-govyadina-kiev-1.jpg";
        meat.name = "Говядина";
        meat.unit = "кг";
        meat.manufacturerName = "ИП Краковский И.А.";
        meat.price = 53800;

        peach.categoryId = 1;
        peach.id = 3;
        peach.imageUrl = "https://www.freshmarket.ru/files/637183111/persiki,w_269,_small.jpg";
        peach.name = "Персик";
        peach.unit = "кг";
        peach.manufacturerId = 1;
        peach.manufacturerName = "ИП Кобылинская И.А.";
        peach.price = 10000;

        products.add(tomato);
        products.add(apple);
        products.add(fish);
        products.add(meat);
        products.add(cucumber);
        products.add(peach);
    }

    @SuppressLint("StaticFieldLeak")
    private void addProductToBasket() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }
        }.execute();
    }

}
