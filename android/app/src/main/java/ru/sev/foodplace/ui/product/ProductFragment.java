package ru.sev.foodplace.ui.product;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import ru.sev.foodplace.App;
import ru.sev.foodplace.R;
import ru.sev.foodplace.data.entity.Product;
import ru.sev.foodplace.ui.base.BaseFragment;

public class ProductFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    private ConstraintLayout filtersSearchLayout;

    private EditText searchEditText;

    private ImageButton searchImageButton;

    private ImageButton showFiltersImageButton;

    private Button searchFilterButton;

    private SwipeRefreshLayout productSwipeRefreshLayout;

    private RecyclerView productRecyclerView;

    private Spinner categorySpinner;

    private Spinner manufacturerSpinner;

    private ProductAdapter adapter;

    public ProductFragment() {
        adapter = new ProductAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.products_layout, container, false);
        filtersSearchLayout = mView.findViewById(R.id.filters_search);
        searchEditText = mView.findViewById(R.id.search_editText);
        searchImageButton = mView.findViewById(R.id.search_imageButton);
        showFiltersImageButton = mView.findViewById(R.id.filter_imageButton);
        productSwipeRefreshLayout = mView.findViewById(R.id.products_swipeRefreshLayout);
        productRecyclerView = mView.findViewById(R.id.products_recyclerView);
        searchFilterButton = mView.findViewById(R.id.search_filter_button);
        categorySpinner = mView.findViewById(R.id.category_Spinner);
        manufacturerSpinner = mView.findViewById(R.id.manufacturer_Spinner);

        productSwipeRefreshLayout.setOnRefreshListener(this);

        productRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        productRecyclerView.setAdapter(adapter);

        setListeners();


        return mView;
    }

    public void showData() {
        adapter.setProducts(App.dataOwner.products);
    }

    public void setupSpinners() {

    }

    public void setListeners() {
        searchImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Product> filteredProducts = new ArrayList<>();



            }
        });

        showFiltersImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFiltersSearchLayoutVisible();
            }
        });

        searchFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO собрать фильтры и сделать запрос с ними

                setFiltersSearchLayoutInvisible();
            }
        });
    }

    public void setFiltersSearchLayoutVisible() {
        filtersSearchLayout.setVisibility(View.VISIBLE);
    }

    public void setFiltersSearchLayoutInvisible() {
        filtersSearchLayout.setVisibility(View.GONE);
    }

    @Override
    public void onRefresh() {
        showData();
    }

    public void startRefresh() {
        productSwipeRefreshLayout.setRefreshing(true);
    }

    public void stopRefresh() {
        productSwipeRefreshLayout.setRefreshing(false);
    }
}
