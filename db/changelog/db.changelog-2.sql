--liquibase formatted sql

--changeset nklyshko:table_product_ADD_name_image_id
ALTER TABLE product ADD COLUMN name VARCHAR(255), ADD COLUMN image_id VARCHAR(255);

--changeset nklyshko:table_category
CREATE TABLE category(id INT PRIMARY KEY, name VARCHAR(255));
