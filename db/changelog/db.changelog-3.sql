--liquibase formatted sql

--changeset nklyshko:table_city_products
CREATE TABLE city_products(city_id INT, product_id BIGINT);
