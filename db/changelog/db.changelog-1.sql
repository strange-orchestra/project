--liquibase formatted sql

--changeset nklyshko:table_city
CREATE TABLE city(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255));

--changeset nklyshko:table_address
CREATE TABLE address(id BIGINT AUTO_INCREMENT PRIMARY KEY, city_id INT, latitude DECIMAL(11,8), longitude DECIMAL(11, 8), name VARCHAR(255));

--changeset nklyshko:table_user
CREATE TABLE user(id BIGINT AUTO_INCREMENT PRIMARY KEY, login VARCHAR(255), hashed_password CHAR(40), role VARCHAR(255));

--changeset nklyshko:table_product
CREATE TABLE product(id BIGINT AUTO_INCREMENT PRIMARY KEY, price INT, category_id INT, quantity INT, unit VARCHAR(255));

--changeset nklyshko:table_manufacturer
CREATE TABLE manufacturer(user_id BIGINT PRIMARY KEY, name VARCHAR(255), description TEXT);

--changeset nklyshko:table_delivery_point
CREATE TABLE delivery_point(id BIGINT AUTO_INCREMENT PRIMARY KEY, address_id BIGINT);

--changeset nklyshko:table_shipment_point
CREATE TABLE shipment_point(id BIGINT AUTO_INCREMENT PRIMARY KEY, address_id BIGINT);

--changeset nklyshko:table_shipment_point-manufacturer
CREATE TABLE shipment_points(manufacturer_id BIGINT, point_id BIGINT);

--changeset nklyshko:table_shipment_point_products
CREATE TABLE shipment_point_products(point_id BIGINT, product_id BIGINT);

--changeset nklyshko:table_exchange_point
CREATE TABLE exchange_point(id BIGINT AUTO_INCREMENT PRIMARY KEY, address_id INT);

--changeset nklyshko:table_cart_item
CREATE TABLE cart_item(id BIGINT AUTO_INCREMENT PRIMARY KEY, product_id BIGINT, quantity INT);

--changeset nklyshko:table_customer
CREATE TABLE customer(user_id BIGINT PRIMARY KEY, status VARCHAR(255), delivery_date DATE, delivery_point_id BIGINT, secret_code VARCHAR(255), secret_code_expires DATETIME);

--changeset nklyshko:table_customer_items
CREATE TABLE customer_items(user_id BIGINT, item_id BIGINT);